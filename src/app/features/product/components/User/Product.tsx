import React from "react";

const Product = () => {
  return (
    <div className="product">
      <div>
        <img src="https://m.media-amazon.com/images/I/71XSvDtb9PL._AC_SY679_.jpg" />
        image
      </div>
      <div className="product__detail">
        <p className="product__title">Furniture</p>
        <p className="product__p">description</p>
      </div>
      <p>prezzo</p>
    </div>
  );
};

export default Product;
