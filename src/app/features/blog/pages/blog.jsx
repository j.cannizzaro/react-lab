import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getBlogsRequest } from '../../../store/blog/actions/actions';
import User from '../components/User';
import Product from '../../product/components/User/Product';




class Blog extends Component {
	componentWillMount() {
		
	}

	_getBlogs = () => {
		this.props.getBlogRequest(1);
	}
	_singleItemClick = (id) => {
		console.log('_singleItemClick ----->', id)
	}

	render() {
		const { blogs = []} = this.props;
        
		return (
			<section className="blog">
				<header className="blog__header">Post Header</header>
				<article  className="blog__actions">
					<button onClick={this._getBlogs}>Load Posts</button>
				</article>
				<article className="blog__posts">
					{blogs && blogs.map((blog ) => 
						<User 
							body={blog.body}
							key={blog.id} 
							id={blog.id}
							title={blog.title} 
							detailHandler={(id) => this._singleItemClick(id)}	
						/>
					)}
				</article>
				<Product />
				<Product />
			</section>
		);
	}
}

const mapDispatchToProps = (dispatch, props) => {
	return {
		getBlogRequest: payload => {
			dispatch(getBlogsRequest(payload));
		}
	};
};
const mapStateToProps = state => {
	return {
		blogs: state.blogReducer.blogs
	};
};

Blog.propTypes = {
	dispatch: PropTypes.func
};

export default connect(mapStateToProps, mapDispatchToProps)(Blog);