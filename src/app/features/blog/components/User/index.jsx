import React from 'react';
import PropTypes from 'prop-types';

const User = ({ body = '', title = '', id, detailHandler }) => {
	const clickHandler = (id) => {
		if (detailHandler) detailHandler(id);
	}

	return (
		<div className="user">
			<h2 className="user__title">{title}</h2>
			<p className="user__description">{body}</p>
			<button onClick={() => clickHandler(id)}>detail</button>
		</div>
	);
};

User.propTypes = {
	body: PropTypes.string,
	title: PropTypes.string,
	id: PropTypes.number,
	detailHandler: PropTypes.func
};

export default User;