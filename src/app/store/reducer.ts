import { combineReducers } from 'redux';
import blogReducer from './blog/reducers/reducer';

const rootReducer = combineReducers({
	blogReducer
});

export default rootReducer;