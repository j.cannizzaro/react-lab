import { all } from 'redux-saga/effects';
import watchDemo from './blog/saga/watcher';

export default function* rootSaga() {
	yield all([
        watchDemo()
    ]);
}
