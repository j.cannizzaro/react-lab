import * as types from '../../consts/blogActionTypes';

export const getBlogsRequest = (payload: any) => ({
	type: types.GET_BLOG_REQUEST,
	payload
});
