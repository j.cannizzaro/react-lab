
import * as types from '../../consts/blogActionTypes';

const initState = {
}
// TODO SET TYPES
export default function blogReducer(state = initState, action: any) {
	switch (action.type) {
		case types.GET_BLOG_REQUEST:{
            return {...state}; 
        }
			
		case types.GET_BLOG_SUCCESS: {
            return {...state, blogs: action.blogData };
        }
			
		default:{
            return state;
        }
			
	}
}
