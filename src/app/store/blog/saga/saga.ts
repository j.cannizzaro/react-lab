import { put, call } from 'redux-saga/effects';
import { getBlogs } from '../api/api';
import * as types from '../../consts/blogActionTypes';

export function* getBlogRequestSaga({ payload }: any): any {
	try {
		const blogData: any = yield call(getBlogs, payload);
		yield put({ type: types.GET_BLOG_SUCCESS, blogData });
	} catch (error) {
		yield put({ type: types.GET_BLOG_ERROR, error });
	}
}
