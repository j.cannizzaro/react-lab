import { takeLatest } from 'redux-saga/effects';
import { getBlogRequestSaga } from './saga';
import * as types from '../../consts/blogActionTypes';

export default function* watchDemo() {
	// NEED TO ADD ALL WATCHER 
	yield takeLatest(types.GET_BLOG_REQUEST, getBlogRequestSaga);
}
