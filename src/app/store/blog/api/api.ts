const axios = require('axios');

export const getBlogs = (params: any) => {
	const URL = 'https://jsonplaceholder.typicode.com/posts';

	return axios.get(URL).then((response: any) => {
		return response.data;
	});
};
