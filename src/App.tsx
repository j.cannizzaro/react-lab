
import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';

import Blog from './app/features/blog/pages/blog';

const App = ({ store }: any) => (
	<Provider store={store}>
		<Router>
			<div>
				<Route exact path="/" component={Blog} />
			</div>
		</Router>
	</Provider>
);

App.propTypes = {
	store: PropTypes.object.isRequired
};

export default App;